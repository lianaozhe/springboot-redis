package com.redis.demo.controller;


import com.redis.demo.result.DealResult;
import com.redis.demo.service.IBladeUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author laz
 * @since 2023-03-09
 */
@RestController
@RequestMapping("/bladeUser")
public class BladeUserController {

    @Autowired
    private IBladeUserService bladeUserService;

    @RequestMapping("getById/{id}")
    public DealResult getById(@PathVariable("id")Long id){
        return bladeUserService.getById(id);
    }

}

