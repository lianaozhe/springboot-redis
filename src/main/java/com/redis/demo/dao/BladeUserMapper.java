package com.redis.demo.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.redis.demo.entity.BladeUser;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author laz
 * @since 2023-03-09
 */
public interface BladeUserMapper extends BaseMapper<BladeUser> {

}
