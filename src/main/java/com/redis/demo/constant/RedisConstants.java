package com.redis.demo.constant;

/**
 * @Author: laz
 * @CreateTime: 2023-03-09  15:40
 * @Version: 1.0
 */
public class RedisConstants {

    public static final String CACHE_USER_KEY = "cache:USER:";
}
