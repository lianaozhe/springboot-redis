package com.redis.demo.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.redis.demo.entity.BladeUser;
import com.redis.demo.result.DealResult;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author laz
 * @since 2023-03-09
 */
public interface IBladeUserService extends IService<BladeUser> {



    DealResult getById(Long id);
}
