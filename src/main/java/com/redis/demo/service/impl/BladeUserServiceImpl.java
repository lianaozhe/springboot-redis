package com.redis.demo.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.redis.demo.constant.RedisConstants;
import com.redis.demo.dao.BladeUserMapper;
import com.redis.demo.entity.BladeUser;
import com.redis.demo.result.DealResult;
import com.redis.demo.service.IBladeUserService;
import com.redis.demo.status.CacheNameStatus;
import com.redis.demo.utils.RedisUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author laz
 * @since 2023-03-09
 */
@Service
public class BladeUserServiceImpl extends ServiceImpl<BladeUserMapper, BladeUser> implements IBladeUserService {


    @Autowired
    private  RedisUtils redisUtils;

//    @Override
//    public DealResult getById(Long id) {
//
//        String userKey = RedisConstants.CACHE_USER_KEY+id;
//        Object user = redisUtils.get(userKey);
//        if (!ObjectUtils.isEmpty(user)){
//
//            return DealResult.data(JSONUtil.toBean(JSONUtil.toJsonStr(user),BladeUser.class));
//        }
//
//        BladeUser bladeUser = baseMapper.selectById(id);
//        redisUtils.set(userKey, JSON.toJSONString(bladeUser));
//        return DealResult.data(bladeUser);
//    }

    @Cacheable(cacheNames = CacheNameStatus.BLADE_USER,keyGenerator = CacheNameStatus.KEY_GENERATOR)
    @Override
    public DealResult getById(Long id) {
        BladeUser bladeUser = baseMapper.selectById(id);
        return DealResult.data(bladeUser);
    }
}
