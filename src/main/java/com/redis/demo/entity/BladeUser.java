package com.redis.demo.entity;

import java.util.Date;
import java.io.Serializable;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author laz
 * @since 2023-03-09
 */
@Data
@Accessors(chain = true)
@TableName("blade_user")
public class BladeUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;
    /**
     * 租户ID
     */
    @TableField("tenant_id")
    private String tenantId;
    /**
     * 用户编号
     */
    private String code;
    /**
     * 用户平台
     */
    @TableField("user_type")
    private Integer userType;
    /**
     * 账号
     */
    private String account;
    /**
     * 密码
     */
    private String password;
    /**
     * 昵称
     */
    private String name;
    /**
     * 真名
     */
    @TableField("real_name")
    private String realName;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 手机
     */
    private String phone;
    /**
     * 生日
     */
    private Date birthday;
    /**
     * 性别
     */
    private Integer sex;
    /**
     * 角色id
     */
    @TableField("role_id")
    private String roleId;
    /**
     * 部门id
     */
    @TableField("dept_id")
    private String deptId;
    /**
     * 岗位id
     */
    @TableField("post_id")
    private String postId;
    /**
     * 创建人
     */
    @TableField("create_user")
    private Long createUser;
    /**
     * 创建部门
     */
    @TableField("create_dept")
    private Long createDept;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 修改人
     */
    @TableField("update_user")
    private Long updateUser;
    /**
     * 修改时间
     */
    @TableField("update_time")
    private Date updateTime;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 是否已删除
     */
    @TableField("is_deleted")
    private Integer isDeleted;


}
