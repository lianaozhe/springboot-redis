package com.redis.demo.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MapUtil {

	private static Logger logger = LoggerFactory.getLogger(MapUtil.class);

	/** 
	* <p>Title: getAttrFromModel </p>
	* <p>Description: java中遍历实体类，获取属性名和属性值，并转换成map </p>
	* @param model 必须是实体类，不得是map之类的
	* @return Map<String,Object>
	* @author yssheep 2019年1月18日 上午11:12:14 
	*/ 
	public static Map<String, Object> getAttrFromModel(Object model) {

		if (model == null) {
			return null;
		}
		Map<String, Object> result = new HashMap<String, Object>();

		try {
			Class cls = model.getClass();
			Field[] fields = cls.getDeclaredFields();
			for (int i = 0; i < fields.length; i++) {
				Field f = fields[i];
				f.setAccessible(true);

				if (null != f.get(model)) {
					result.put(f.getName(), f.get(model));
				}

				
				// System.out.println("属性名:" + f.getName() + " 属性值:" + f.get(model));
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {

			logger.error(e.getLocalizedMessage(), e);

			return null;
		}

		return result;
	}

	// ==============================Test=============================================
	/** 测试 */
	/*public static void main(String[] args) {

		SysRoleVo sysRoleVo = new SysRoleVo();

		sysRoleVo.setId((long) 1234);
		sysRoleVo.setName("good man");

		Map<String, Object> result = getAttrFromModel(sysRoleVo);

		System.out.print(result);

	}*/
}
