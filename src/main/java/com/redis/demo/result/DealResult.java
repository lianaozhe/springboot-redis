package com.redis.demo.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: laz
 * @CreateTime: 2022-12-29  11:00
 * @Version: 1.0
 */
@Data
public class DealResult implements Serializable {

    private static final long serialVersionUID = 1L;

    public int code;

    public Object data;

    public String msg;

    public DealResult() {
    }

    public DealResult(int code, Object data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

    public DealResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static DealResult success(String msg){
        return new DealResult(200,msg);
    }

    public static DealResult fail(String msg){
        return new DealResult(400,msg);
    }

    public static DealResult data(Object data){
        return new DealResult(200,data,"查询成功!");
    }
}
